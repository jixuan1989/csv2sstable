generate cassandra sstable by using CQLSSTableWriter.
inspired from : http://www.datastax.com/dev/blog/using-the-cassandra-bulk-loader-updated

To assign your data source, use: -Dcsvfrom=your csv

after generate sstable data in the `output` folder, copy all the .db files into the `your keyspace name/your table name` folder. Then use C*/bin/sstableloader -d ip1,ip2,...ipn /path/.../your keyspace name/your table name
(you can claim just one ip address.)

Enjoy it!