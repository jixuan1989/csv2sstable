/**
 * Created by hxd on 17/4/11.
 */

import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.cassandra.config.DatabaseDescriptor;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

import org.apache.cassandra.config.Config;
import org.apache.cassandra.dht.Murmur3Partitioner;
import org.apache.cassandra.exceptions.InvalidRequestException;
import org.apache.cassandra.io.sstable.CQLSSTableWriter;
public class csv2sstable {
    /** Keyspace name */
    public static final String KEYSPACE = "sagittarius";
    /** Table name */
    public static final String TABLE = "data_float";

    public static final String SCHEMA = String.format("CREATE TABLE %s.%s (\n" +
            "    host text,\n" +
            "    metric text,\n" +
            "    time_slice text,\n" +
            "    primary_time bigint,\n" +
            "    secondary_time bigint,\n" +
            "    value float,\n" +
            "    PRIMARY KEY ((host, metric, time_slice), primary_time)\n" +
            ") WITH CLUSTERING ORDER BY (primary_time ASC)", KEYSPACE, TABLE);

    /**
     * INSERT statement to bulk load.
     * It is like prepared statement. You fill in place holder for each data.
     */
    public static final String INSERT_STMT = String.format("INSERT INTO %s.%s (" +
            "host, metric, time_slice, primary_time, value" +
            ") VALUES (" +
            "?, ?, ?, ?,  ?" +
            ")", KEYSPACE, TABLE);

    public static void main(String[] args) throws IOException, ParseException {
        Thread.currentThread().setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("oooooooooooops:"+System.currentTimeMillis());
                e.printStackTrace();
            }
        });
        String dir=(System.getProperty("user.dir"));
        String separator=File.separator;
        //DatabaseDescriptor.daemonInitialization();
        //create a CQLSSTableWriter

        //prepare data...
        String  datasource=System.getProperty("csvfrom",dir+"/test.csv").replace("/",File.separator);
        List<String> lines = Files.readAllLines(Paths.get(datasource));
        List<Datum> data= lines.stream().map(line->{
            String[] sl=line.split(",");
            return new Datum(sl[0].trim(), sl[1].trim(), sl[2].trim(), Float.parseFloat(sl[3].trim()));
        }).collect(Collectors.toList());
        IntStream.range(0,data.size()).forEach(i->data.get(i).primary_time=i);//just for generating more data
        lines.clear();
        //begin....
        long begin=System.currentTimeMillis();
        System.out.println("begin:"+begin);
        int j=1;//j is meaningless
        //IntStream.range(1,10).sequential().forEach(j-> {
        new File("output"+j).mkdir();
        CQLSSTableWriter writer = CQLSSTableWriter.builder()
                .inDirectory(new File("output"+j))
                .forTable(SCHEMA)
                .withBufferSizeInMB(Integer.parseInt(System.getProperty("buffer_size_in_mb", "65")))//FIXME!! if the size is 64, it is ok, if it is 128 or larger, boom!!
                .using(INSERT_STMT)
                .withPartitioner(new Murmur3Partitioner()).build();
        IntStream.range(1, 100).sequential().forEach(i ->//because the csv has few data, we have to copy data for reproducing the high CPU and "sleeping" I/O for sstable flushing.
                //if you can not reproducing the problem, increase the IntStream from 100 to more (e.g., 1000)
                {
                    System.out.println(i + ":");
                    data.stream().sequential().forEach((Datum datum) -> {
                        try {
                            writer.addRow(datum.host, datum.metric, datum.time_slice, begin + i * data.size() + datum.primary_time, datum.value);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }
        );
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // });
        long end=System.currentTimeMillis()-begin;
        System.out.println("write data time cost: " +end);
    }
    public static class Datum{
        String host;
        String metric;
        String time_slice;
        long primary_time;
        long secondary_time;
        float value;

        public Datum(String host, String metric, String time_slice,  float value) {
            this.host = host;
            this.metric = metric;
            this.time_slice = time_slice;
            this.value = value;
        }
    }
}
